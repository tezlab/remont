<?php

$emails = array('nntezlab+projects@gmail.com');
$site = 'Искусство комфорта';
$subject = 'Заявка с сайта';


function sendmail($text){
	global $site,$subject,$emails;
	require_once('lib/PHPMailer/class.phpmailer.php');
	$mail = new PHPMailer();
	$mail->From     = "noreply@".$_SERVER['SERVER_NAME'];
	$mail->FromName = iconv('UTF-8','WINDOWS-1251',$site);
	$mail->Subject  = iconv('UTF-8','WINDOWS-1251',$subject);
	$mail->CharSet  = 'Windows-1251';
	$mail->SingleTo	= true;
	$mail->ContentType = 'text/html';
	foreach ($emails as $email){
		$mail->AddAddress($email,$email);
	}
	$template = '
		<html>
			<head><title>%s</title></head>
			<body>
				<h3>%s</h3>
				%s
			</body>
		</html>
	';
	$mail->MsgHTML(iconv('UTF-8','WINDOWS-1251',sprintf($template, $subject, $subject, $text)));
	return $mail->Send();
}

$content = sprintf('
		<p><b>Имя</b>: %s</p>
		<p><b>Телефон</b>: %s</p>
	',$_POST['name'],$_POST['phone']);

sendmail($content);

echo "<h2>Спасибо за оставленную заявку. <br>Мы вам совсем скоро перезвоним!</h2>";

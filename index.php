<!DOCTYPE html>
<html>
    <head>

        <!-- /.website title -->
        <title>Искусство комфорта | Дизайн и отделка</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <!-- CSS Files -->
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet" media="screen">
        <link href="css/owl.theme.css" rel="stylesheet">
        <link href="css/owl.carousel.css" rel="stylesheet">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">

        <!-- Colors -->
        <link href="css/css-index.css" rel="stylesheet" media="screen">
        <!-- <link href="css/css-index-green.css" rel="stylesheet" media="screen"> -->
        <!-- <link href="css/css-index-purple.css" rel="stylesheet" media="screen"> -->
        <!-- <link href="css/css-index-red.css" rel="stylesheet" media="screen"> -->
        <!-- <link href="css/css-index-orange.css" rel="stylesheet" media="screen"> -->
        <!-- <link href="css/css-index-yellow.css" rel="stylesheet" media="screen"> -->

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Comfortaa|News+Cycle|Poiret+One|Play" rel="stylesheet">
    </head>

    <body data-spy="scroll" data-target="#navbar-scroll">

        <!-- /.preloader -->
        <div id="preloader"></div>
        <div id="top"></div>

        <!-- /.parallax full screen background image -->
        <div class="fullscreen landing parallax" style="background-image:url('images/bg.jpg');" data-img-width="2000" data-img-height="1000" data-diff="100">

            <div class="overlay2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">

                            <!-- /.logo -->
                            <div class="logo wow fadeInDown"> 
                                <a href="/">
                                    <i class="pe-7s-home"></i>
                                    <span class=logo-name-wrap>
                                        Искусство комфорта
                                        <small>Дизайн интерьера и отделка "под ключ"</small>
                                    </span>
                                </a>
                                
                            </div>

                            <!-- /.main title -->
                            <h1 class="wow fadeInLeft">
                                Занимаемся дизайном интерьера и отделкой в Санкт-Петербурге
                            </h1>

                            <!-- /.header paragraph -->
                            <div class="landing-text wow fadeInUp">
                                <p>
                                    Получите красивую и качественную отделку Вашего помещения от команды профессионалов.
                                </p>
                            </div>
                            <div class="landign-text wow fadeInUp">
                                <p>
                                    - Придя один раз к нам, Вы уже станете нашим <strong class="highlight">партнером</strong>. <br>
                                    - Хотите узнать как?<br>
                                    - <a href="#uds"><strong>Хочу &rarr;</strong></a>
                                </p>
                            </div>
                            
                        </div> 

                        <!-- /.signup form -->
                        <div class="col-md-5">
                            <div class="phone">
                                <h3>Будем рады Вашему звонку</h3>
                                <a class="wow fadeInUp" href="tel:+78129927523" onclick="yaCounter44768167.reachGoal('phone'); return true;" data-wow-delay="0.8s"><strong>+7 (812) 992-75-23</strong></a>
                                <a class="wow fadeInUp" href="https://www.instagram.com/golubev3250/" onclick="yaCounter44768167.reachGoal('instagram'); return true;" target="_blank" data-wow-delay="0.6s"><i class="fa fa-instagram"></i></a>
                                <a class="wow fadeInUp" href="https://vk.com/id278907202" onclick="yaCounter44768167.reachGoal('vk'); return true;" target="_blank" data-wow-delay="0.4s"><i class="fa fa-vk"></i></a>
                            </div>
                            <div class="signup-header wow fadeInUp">
                                <h3 class="form-title text-center">Бесплатная консультация</h3>
                                <form class="form-header"  action="contactengine.php" role="form" method="POST" id="contact-form">
                                    <div class="form-group">
                                        <input class="form-control input-lg" name="name" id="Name" type="text" placeholder="Имя" required>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control input-lg" name="phone" id="Phone" type="text" placeholder="Телефон" required>
                                    </div>
                                    <div class="form-group last">
                                        <input type="submit" onclick="yaCounter44768167.reachGoal('bidform'); return true;" class="btn btn-warning btn-block btn-lg" value="Узнать стоимость">
                                    </div>
                                    <p class="privacy text-center">Наш специалист очень скоро свяжется с Вами.</p>
                                </form>
                            </div>				

                        </div>
                    </div>
                </div> 
            </div> 
        </div>
        <!--
        <div class="message">
            <div class="container">
                <div class="message-title">
                    Ремонт - это искусство
                </div>
            </div>
        </div>-->
        <div id="design">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 service-img">
                        <img src="/images/design.jpg">
                    </div>
                    <div class="col-md-6 service-info">
                        <h3 class="subtitle-h3">Что мы делаем?</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="subtitle-h4">Дизайн</h4>
                                <ul>
                                    <li>Проектирование жилых и административных зданий, в том числе перепланировка  </li>
                                    <li>Реконструкция и реставрация</li>
                                    <li>Дизайн пространства</li>
                                    <li>Индивидуальный подбор материала, формы и цвета на любой вкус</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="subtitle-h4">Отделка</h4>
                                <ul>
                                    <li>Малярные работы</li>
                                    <li>Все виды стяжек пола (мокрая, полусухая, сухая)</li>
                                    <li>Штукатурные работы всеми способами (ручной, механизированный)<span class="highlight">*</span></li>
                                    <li>Монтаж конструкций с ГКЛ любой сложности</li>
                                    <li>Прочие виды отделки</li>
                                </ul>
                                <p>
                                    <span class="highlight">*</span>Подробнее можете узнать на найшей странице <a href="http://штукатурка-в-питере.рф">штукатурка-в-питере.рф</a>
                                </p>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="row">                
                    <div class="col-md-6 service-info">
                        <h3 class="subtitle-h3">Почувствуйте тепло, тишину и уют...</h3>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="subtitle-h4">Комфорт</h4>
                                <ul>
                                    <li>Вентиляция и кондиционирование</li>
                                    <li>Звукоизоляция</li>
                                    <li>Электромонтаж</li>
                                    <li>Водоснабжение</li>
                                </ul>
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="subtitle-h4">Отопление</h4>
                                <ul>
                                    <li>Конвекторы</li>
                                    <li>Радиаторы</li>
                                    <li>Отопительные котлы</li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-6 service-img">
                        <img src="/images/komfort.jpg">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 service-img">
                        <img src="/images/smart_home.jpg">
                    </div>
                    <div class="col-md-6 service-info">
                        <h3 class="subtitle-h3"></h3>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="subtitle-h4">Управление жилым пространством и безопасность</h4>
                                <ul>
                                    <li>Видеонаблюдение</li>
                                    <li>Системы охраны</li>
                                    <li>Умный дом</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="subtitle-h4">Соответствие дизайна и реализации</h4>
                                <ul>
                                    <li>Авторский надзор</li>
                                    <li>Технический надзор</li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

        <!-- /.feature section -->
        <div id="feature" class="fullscreen landing parallax" style="background-image:url('images/bg4.jpg');">
            <div class="overlay2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1 col-sm-12 text-center feature-title">

                            <!-- /.feature title -->
                            <h2 class="wow fadeInLeft">С нами вы получаете</h2>
                        </div>
                    </div>
                    <div class="row">
                        
                        <div class="col-md-12">

                            <!-- /.feature 1 -->
                            <div class="col-md-3 col-sm-6 feat-list">
                                <i class="pe-7s-lock pe-5x pe-va wow fadeInUp" data-wow-delay="0.4s"></i>
                                <div class="inner">
                                    <h4>Вывоз мебели</h4>
                                    <p>Ваша мебель хранится на нашем специальном складе на время ремонта</p>
                                </div>
                            </div>
                            

                            <!-- /.feature 2 -->
                            <div class="col-md-3 col-sm-6 feat-list">
                                <i class="pe-7s-paint pe-5x pe-va wow fadeInUp"></i>
                                <div class="inner">
                                    <h4>Гарантия</h4>
                                    <p>
                                        Внимание к деталям и сдача в срок благодаря 12 летнему стажу в сфере ремонта                                    
                                    </p>
                                </div>
                            </div>

                            <!-- /.feature 3 -->
                            <div class="col-md-3 col-sm-6 feat-list">
                                <i class="pe-7s-cash pe-5x pe-va wow fadeInUp" data-wow-delay="0.2s"></i>
                                <div class="inner">
                                    <h4>Экономия до 25%</h4>
                                    <p>за счет оптимального планирования работы над проектом</p>
                                </div>
                            </div>                       

                            <!-- /.feature 4 -->
                            <div class="col-md-3 col-sm-6 feat-list">
                                <i class="pe-7s-trash pe-5x pe-va wow fadeInUp" data-wow-delay="0.6s"></i>
                                <div class="inner">
                                    <h4>Уборка после ремонта</h4>
                                    <p>Будете довольны еще и чистотой</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="uds">
            <div class="container">
                <div class="row  text-center">
                    <div class="col-md-12">
                        <h2 class="subtitle wow fadeInLeft">Хотите получить бонусы и стать партнером?</h2>
                        <div class="title-line wow fadeInRight"></div>
                        <p>Достаточно сделать 2 шага</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-offset-2">
                        <div class="step-title subtitle-h4">
                            1. Установите приложение
                        </div>
                        <div class="step-info">
                            Скачайте приложение <span class="highlight">UDS Game</span> <br>на свой смартфон 
                            <a target="_blank" href="//play.google.com/store/apps/details?id=com.setinbox.game"><i class="android-icon"></i></a>
                            <a target="_blank" href="https://itunes.apple.com/ru/app/game-united-discount-system/id954809132?mt=8"><i class="apple-icon"></i></a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-4">
                                <img src="/images/qr.jpg" class="qr-code"/>
                            </div>
                            <div class="col-md-8">
                                <div class="step-title subtitle-h4">
                                    2. Начните играть
                                </div>
                                <div class="step-info">
                                    Просканируйте QR-код или <br>введите код вручную <span class="highlight">ndbw1801</span>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>

        <!-- /.testimonial section -->
        <?php if(false){?>
        <div id="testi">
            <div class="container">
                <div class="text-center">
                    <h2 class="wow fadeInLeft">Отзывы</h2>
                    <div class="title-line wow fadeInRight"></div>
                </div>
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">	
                        <div id="owl-testi" class="owl-carousel owl-theme wow fadeInUp">

                            <!-- /.testimonial 1 -->
                            <div class="testi-item">
                                <div class="client-pic text-center">

                                    <!-- /.client photo -->
                                    <img src="images/testi1.png" alt="client">
                                </div>
                                <div class="box">

                                    <!-- /.testimonial content -->
                                    <p class="message text-center">"We are very happy and satisfied with Backyard service. Our account manager is efficient and very knowledgeable. It was able to create a vast fan base within very short period of time. We would highly recommend Backyard App to anyone."</p>
                                </div>
                                <div class="client-info text-center">

                                    <!-- /.client name -->
                                    Jennifer Lopez, <span class="company">Microsoft</span>	
                                </div>
                            </div>		

                            <!-- /.testimonial 2 -->
                            <div class="testi-item">
                                <div class="client-pic text-center">

                                    <!-- /.client photo -->
                                    <img src="images/testi2.png" alt="client">
                                </div>
                                <div class="box">

                                    <!-- /.testimonial content -->
                                    <p class="message text-center">"Everything looks great... Thanks for the quick revision turn around. We were lucky to find you guys and will definitely be using some of your other services in the near future."</p>
                                </div>
                                <div class="client-info text-center">

                                    <!-- /.client name -->
                                    Mike Portnoy, <span class="company">Dream Theater</span>	
                                </div>
                            </div>				

                            <!-- /.testimonial 3 -->
                            <div class="testi-item">
                                <div class="client-pic text-center">

                                    <!-- /.client photo -->
                                    <img src="images/testi3.png" alt="client">
                                </div>
                                <div class="box">

                                    <!-- /.testimonial content -->
                                    <p class="message text-center">"Overall, the two reports were very clear and helpful so thank you for the suggestion to do the focus group. We are currently working with our developer to implement some of these suggestions."</p>
                                </div>
                                <div class="client-info text-center">

                                    <!-- /.client name -->
                                    Jennifer Love Hewitt, <span class="company">Lead Vocal</span>	
                                </div>
                            </div>			

                        </div>
                    </div>	
                </div>	
            </div>
        </div>
        
        <?php }?>

        <!-- /.contact section -->
        <div id="contact">
            <div class="contact fullscreen parallax" style="background-image:url('images/design2.jpg');" data-img-width="2000" data-img-height="1334" data-diff="100">
                <div class="overlay2">
                    <div class="container">
                        <div class="row contact-row">

                            <!-- /.address and contact -->
                            <div class="col-md-8 col-sm-5 contact-left wow fadeInUp">
                                <h2><span class="highlight">Получите</span> больше информации</h2>
                                <ul class="ul-address">
                                    <li><i class="pe-7s-phone"></i><a href="tel:78129927523">+7 (812) 992-75-23</a>
                                    </li>
                                    <li><i class="pe-7s-look"></i><a href="/">искусство-комфорта.рф</a></li>
                                    <li><i class="pe-7s-map"></i>
                                    <a>194044, Санкт-Петербург г, Большой Сампсониевский, <br>дом № 19, корпус Лит А, кв.Пом

5-Н</a></li>
                                    
                                </ul>	

                            </div>

                            <!-- /.contact form -->
                            <div class="col-md-4 col-sm-7 contact-right">
                                <form method="POST" id="contact-form" class="form-horizontal" action="contactengine.php">
                                    <div class="form-group">
                                        <input type="text" name="name" id="Name" class="form-control wow fadeInUp" placeholder="Ваше имя" required/>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="phone" id="Phone" class="form-control wow fadeInUp" placeholder="Ваш телефон" required/>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" name="submit" value="Перезвонить мне" class="btn btn-success wow fadeInUp" />
                                    </div>
                                    <div class="form-group">
                                        <p class="highlight">Ваши данные защищены</p>
                                    </div>
                                </form>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.footer -->
        <footer id="footer">
            <div class="container">
                <div class="col-sm-4 col-sm-offset-4">
                    <!-- /.social links -->
                    <div class="social text-center">
                        <ul>
                            <li><a class="wow fadeInUp" target="_blank" onclick="yaCounter44768167.reachGoal('instagram'); return true;" href="https://www.instagram.com/golubev3250/" data-wow-delay="0.6s"><i class="fa fa-instagram"></i></a></li>
                            <li><a class="wow fadeInUp" target="_blank" onclick="yaCounter44768167.reachGoal('vk'); return true;" href="https://vk.com/id278907202" data-wow-delay="0.4s"><i class="fa fa-vk"></i></a></li>
                        </ul>
                    </div>
                    <div class="text-center wow fadeInUp" style="font-size: 14px;">Разработано студией  <a href="http://tezlab.ru" onclick="yaCounter44768167.reachGoal('tezlab'); return true;" target="_blank">Tezlab</a></div>
                    <a href="#" class="scrollToTop"><i class="pe-7s-up-arrow pe-va"></i></a>
                </div>	
            </div>	
        </footer>
        

        <!-- /.javascript files -->
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/custom.js"></script>
        <script src="js/jquery.sticky.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script>
            new WOW().init();
            $('body').on('submit', '#contact-form', function(e){
                e.preventDefault();
                var form = $(this);
                $.ajax({
                    url: $(this).attr('action'),
                    method: 'post',
                    data: {name: $('#Name').val(), phone: $('#Phone').val()},
                    success: function(data){
                        //$('.signup-header').addClass('contact-right-bg');
                        form.parent().html(data);
                    }
                })
            });
        </script>

        <!-- Yandex.Metrika counter -->
        <script type="text/javascript">
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter44768167 = new Ya.Metrika({
                            id:44768167,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true,
                            webvisor:true,
                            trackHash:true
                        });
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://mc.yandex.ru/metrika/watch.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/44768167" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
    </body>
</html>